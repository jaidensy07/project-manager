from datetime import date
from django.db import models

from projects.models import AUTH_USER


class Task(models.Model):
    topic = models.CharField(max_length=200)
    date = models.DateField(auto_now=True)
    method = models.CharField(max_length=200, blank=True, null=True)
    link = models.URLField(null=True, blank=True)
    minutes = models.IntegerField(blank=True, null=True)
    understanding = models.BooleanField(default=False)
    description = models.TextField(blank=True, null=True)

    project = models.ForeignKey(
        "projects.Project",
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    assignee = models.ForeignKey(
        AUTH_USER,
        null=True,
        related_name="tasks",
        on_delete=models.SET_NULL,
    )

    def __str__(self):
        return self.topic
